import sa from "superagent";

import { handleSuccess, handleError } from "_utils";


export const Register = user =>
  sa.post("api/auth/register/")
    .send(user)
    .then(handleSuccess)
    .catch(handleError)

export const Login = user =>
  sa.post("api/auth/login/")
    .send(user)
    .then(handleSuccess)
    .catch(handleError);

export const Logout = access_token =>
  sa.get("api/auth/logout/")
    .set("Authorization", `Bearer ${access_token}`)
    .then(handleSuccess)
    .catch(handleError);

export const Refresh = refresh_token =>
  sa.get("api/auth/logout/")
    .set("Authorization", `Bearer ${refresh_token}`)
    .then(handleSuccess)
    .catch(handleError);

export const ValidateToken = access_token =>
  sa.get("api/auth/validate_token/")
    .set("Authorization", `Bearer ${access_token}`)
    .then(handleSuccess)
    .catch(handleError)

