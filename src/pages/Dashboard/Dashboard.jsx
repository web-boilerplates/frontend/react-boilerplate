import React from "react";
import PropTypes from "prop-types";
import { useSessionStatus } from "_utils";


const Dashboard = ({history}) => {
  const sessionStatus = useSessionStatus();

  if (!sessionStatus) {
    history.push("/login");
  }

  return (
    <main>
      <h4>Welcome {sessionStatus.username}</h4>
      <p>Prove that we are logged in pls</p>
    </main>
  );
};
Dashboard.propTypes = {
  history: PropTypes.any
};

export default Dashboard;
