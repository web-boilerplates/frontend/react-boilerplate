import React from "react";
import PropTypes from "prop-types";

import LoginForm from "_components/LoginForm";

const LoginRegisterPage = ({history}) => {
  return(
    <main>
      <LoginForm history={history} />
    </main>
  );
};
LoginRegisterPage.propTypes = {
  history: PropTypes.any
};

export default LoginRegisterPage;
