import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { deleteSession } from "_src/sessions";

const LogoutPage = ({history}) => {
  useEffect(
    () => {
      deleteSession();
      history.push("/login");
    },[history]
  );

  return <div>Logged out!</div>;
};
LogoutPage.propTypes = {
  history: PropTypes.any
};
