import { useState, useEffect, useContext } from "react";
import { SessionContext } from "_src/sessions";
import { ValidateToken } from "_api/auth";

//export const checkProps = (component, expectedProps) => {
  //let propsError = checkPropTypes(component.propTypes, expectedProps, "props", component.name);
  //return propsError;
//};

export const capitalize = str => str[0].toUpperCase() + str.slice(1);


/* API Utils */
export const handleSuccess = res => res.body;

export const handleError = err => {
  if (err.response) { throw err.response; }
  else {
    const res = { status: 500, body: { message: "Internal Server Error" } };
    throw res;
  }
};

/* Custom Hook to ensure valid session */
export const useSessionStatus = () => {
  const [isSessionValid, setIsSessionValid] = useState(null);
  const { access_token } = useContext(SessionContext);

  useEffect(async () => {
    await ValidateToken(access_token)
      .then(() => {
        setIsSessionValid(true);
      })
      .catch(() => {
        setIsSessionValid(false);
      });
  });

  return isSessionValid;
};
