import React, { useState, useEffect } from "react";
import { Router, Switch, Route } from "react-router-dom";
import { createBrowserHistory } from "history";
import { SessionContext, getSession } from "_src/sessions";

import Header from "_components/Header";
import Footer from "_components/Footer";

/* Pages */
import HomePage from "_pages/HomePage";
import Dashboard from "_pages/Dashboard";
import LoginRegisterPage from "_pages/LoginRegisterPage";
import NotFoundPage from "_pages/NotFoundPage";

const history = createBrowserHistory();

const App = () => {
  const [session, setSession] = useState(getSession());

  useEffect(
    () => {
      setSession(getSession());
    },
    [session]
  );

  return (
    <SessionContext.Provider value={session}>
      <Router history={history}>
        <Header />
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/dashboard" component={Dashboard} />
          <Route path="/login" component={LoginRegisterPage} />
          <Route path="/register" component={LoginRegisterPage} />
          <Route path="*" component={NotFoundPage} />
        </Switch>
        <Footer />
      </Router>
    </SessionContext.Provider>
  );
};

export default App;
