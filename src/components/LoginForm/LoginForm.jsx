import React, { useState, useContext } from "react";
import PropTypes from "prop-types";
import { SessionContext } from "_src/contexts";
import { Login } from "_api/auth";

const LoginForm = ({history}) => {
  const [vals, setVals] = useState({username: "", password: ""})
  const [loading, setLoading] = useState(false);
  const { setSessionTokens } = useContext(SessionContext);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    await Login(vals)
      .then(setSessionTokens);
    history.push("/dashboard");
    setLoading(false);
  };

  const handleChange = (e) => {
    setVals({...vals, [e.target.name]: e.target.value});
  };

  if (loading) {
    return <h4>Logging in...</h4>;
  }

  return (
    <form onSubmit={handleSubmit}>
      <label>Username:
        <input type="text" name="username" value={vals.username} onChange={handleChange} />
      </label>
      <label>Password:
        <input type="password" name="password" value={vals.password} onChange={handleChange} />
      </label>
      <input type="submit" value="Login" />
    </form>
  );
};
LoginForm.propTypes = {
  history: PropTypes.any
};

export default LoginForm;
