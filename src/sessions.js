import React from "react";

export const deleteSession = () => {
  localStorage.removeItem("jwt_access");
  localStorage.removeItem("jwt_refresh");
};

export const setSession = ({access, refresh}) => {
  localStorage.setItem("jwt_access", access);
  localStorage.setItem("jwt_refresh", refresh);
};

export const getSession = () => ({
  access_token: localStorage.getItem("jwt_access"),
  refresh_token: localStorage.getItem("jwt_refresh")
});

export const SessionContext = React.createContext(getSession());
