/* eslint-disable no-var */
const webpack = require("webpack");
const { merge } = require("webpack-merge");

const helpers = require("./helpers");
const common = require("./webpack.common");

/* Plugins */


module.exports = merge(common, {
  mode: "development",
  devtool: "inline-source-map",
  output: {
    filename: "[name].bundle.js",
    publicPath: "/",
    path: helpers.root("dist")
  },
  devServer: {
    contentBase: helpers.root("dist"),
    historyApiFallback: true,
    host: "0.0.0.0",
    port: 3000,
    hot: true,
    compress: true
  }
});
